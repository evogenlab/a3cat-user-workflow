rule busco:
    '''
    Run BUSCO on a single assembly with a single lineage.
    The entire results folder is stored as an archive except for blast_db and
    the redundant genome sequence in blast_output; only directly relevant
    files are retained as plain files.
    This command is the exact same used to compute BUSCO scores for A3Cat.
    '''
    input:
        assembly_file = lambda wildcards: config['assemblies'][wildcards.assembly_name]['File Path']
    output:
        tarball = 'results/{assembly_name}/busco/{lineage}/results.tar.gz',
        table = 'results/{assembly_name}/busco/{lineage}/full_table.tsv',
        missing_busco = 'results/{assembly_name}/busco/{lineage}/missing_busco_list.tsv',
        summary = 'results/{assembly_name}/busco/{lineage}/short_summary.txt'
    log:
        'logs/{assembly_name}/busco_{lineage}.log'
    benchmark:
        'benchmarks/{assembly_name}/busco_{lineage}.tsv'
    conda:
        '../envs/assessment.yaml'
    shadow: 'full'
    threads: 
        get_threads('busco')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('busco', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('busco', attempt)
    params:
        mode = 'genome',
        output_root = lambda wildcards: f'results/{wildcards.assembly_name}/busco',
        output_dir = lambda wildcards: f'tmp_{wildcards.lineage}',
        final_dir = lambda wildcards: f'results/{wildcards.assembly_name}/busco/{wildcards.lineage}',
        tmp_tarball = lambda wildcards: f'tmp_{wildcards.lineage}.tar.gz'
    shell:
        'busco --force '
        '--in {input.assembly_file} '
        '--lineage_dataset {wildcards.lineage} '
        '--out_path {params.output_root} '
        '--out {params.output_dir} '
        '--mode {params.mode} '
        '--cpu {threads} '
        '> {log} 2>&1;'
        'mv {params.output_root}/{params.output_dir}/* {params.final_dir} >> {log} 2>&1;'
        'rm -rf {params.output_root}/{params.output_dir} >> {log} 2>&1;'
        'rm -rf {params.final_dir}/blast_db 2>> {log};'
        'rm -rf {params.final_dir}/run_*/blast_output/sequences 2>> {log};'
        'tar cvf - -C {params.output_root} {wildcards.lineage} 2>> {log} | '
        'xz -T {threads} -zkf -9 - > {params.tmp_tarball} 2>> {log};'
        'mv {params.tmp_tarball} {output.tarball} 2>> {log};'
        'rm -rf {params.final_dir}/logs 2>> {log};'
        'rm -rf {params.final_dir}/prodigal_output 2>> {log};'
        'rm -f {params.final_dir}/short_summary*.txt 2>> {log};'
        'mv -f {params.final_dir}/run_*/*.{{txt,tsv}} {params.final_dir} 2>> {log};'
        'rm -rf {params.final_dir}/run_* 2>> {log};'


rule assembly_stats:
    '''
    Compute assembly metrics like size, N50, L50, and number of contigs with the assembly-stats python package.
    '''
    input:
        assembly_file = lambda wildcards: config['assemblies'][wildcards.assembly_name]['File Path']
    output:
        'results/{assembly_name}/assembly_stats.json'
    log:
        'logs/{assembly_name}/assembly_stats.txt'
    conda:
        '../envs/assessment.yaml'
    threads: 
        get_threads('assembly_stats')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('assembly_stats', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('assembly_stats', attempt)
    shell:
        'assembly_stats {input} > {output} 2> {log}'
