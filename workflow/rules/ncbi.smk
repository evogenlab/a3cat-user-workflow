rule download_ete3_db:
    '''
    Download and store the database for ete3. Specifying the path ensures that
    a new up-to-date database is created with the workflow.
    '''
    output:
        'results/.ete3db'
    log:
        'logs/download_ete3_db.txt'
    conda:
        '../envs/ncbi.yaml'
    script:
        '../scripts/download_ete3_db.py'


rule get_taxonomy:
    '''
    Retrieve taxonomy information from NCBI or from the manually specified taxonomy if
    the focal species is not in NCBI taxonomy.
    '''
    input:
        rules.download_ete3_db.output
    output:
        'results/{assembly_name}/taxonomy.json'
    log:
        'logs/{assembly_name}/get_taxonomy.txt'
    conda:
        '../envs/ncbi.yaml'
    threads: 
        get_threads('get_taxonomy')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('get_taxonomy', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('get_taxonomy', attempt)
    params:
        assembly_info = lambda wildcards: config['assemblies'][wildcards.assembly_name],
        taxid_header = SPECIES_TAXID,
        taxonomy_header = TAXONOMY_FILE
    script:
        '../scripts/get_taxonomy.py'
