from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider

HTTP = HTTPRemoteProvider()

rule get_latest_table:
    '''
    Download the latest version of the A3Cat summary table.
    '''
    input: 
        HTTP.remote(config['a3cat_latest_url'])
    output:
        'resources/a3cat.tsv'
    log:
        'logs/get_latest_table.txt'
    threads: 
        get_threads('get_latest_table')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('get_latest_table', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('get_latest_table', attempt)
    shell:
        'wget -O {output} {input} 2> {log}'


rule generate_comparison_data:
    '''
    Retrieve data for the relevant taxonomical level from the A3Cat and combine it with data
    for the focal assembly.
    '''
    input:
        a3cat = rules.get_latest_table.output,
        busco_summary_files = lambda wildcards: expand(rules.busco.output.summary, 
                                                       lineage=config['assemblies'][wildcards.assembly_name][LINEAGE_DATASETS],
                                                       assembly_name=wildcards.assembly_name),
        taxonomy = rules.get_taxonomy.output,
        assembly_stats = rules.assembly_stats.output
    output:
        'results/{assembly_name}/.comparison_{level}.tsv'
    log:
        'logs/{assembly_name}/generate_comparison_data_{level}.txt'
    threads: 
        get_threads('generate_comparison_data')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('generate_comparison_data', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('generate_comparison_data', attempt)
    params:
        assembly_info = lambda wildcards: config['assemblies'][wildcards.assembly_name],
        lineage_header = LINEAGE_DATASETS
    script:
        '../scripts/generate_comparison_data.py'
