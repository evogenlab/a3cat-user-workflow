import logging
import os
from ete3 import NCBITaxa
from utils import setup_logging, create_dir


# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')
    ete3_db_path = snakemake.output[0]

    logging.info('Downloading ete3 database')
    DB_PATH = os.path.realpath(ete3_db_path)
    create_dir(os.path.dirname(DB_PATH))
    if not os.path.isfile(DB_PATH):
        with open(DB_PATH, 'w') as dbfile:
            logging.info(f'Initialized empty ete3 db file at <{DB_PATH}>')
    ncbi = NCBITaxa(dbfile=DB_PATH)
    if os.path.isfile('taxdump.tar.gz'):
        os.remove('taxdump.tar.gz')
    logging.info('Successfully downloaded ete3 database')
