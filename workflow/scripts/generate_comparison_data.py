import logging
import os
import re
from utils import setup_logging, load_json_to_dict, nameify

ASSEMBLY_SIZE_HEADER = 'Total length'
SCAFFOLD_N50_HEADER = 'Scaffold N50'
SCAFFOLD_L50_HEADER = 'Scaffold L50'
BUSCO_COMPLETE_HEADER = '{lineage} Complete BUSCO'
BUSCO_FRAGMENTED_HEADER = '{lineage} Fragmented BUSCO'
BUSCO_DUPLICATED_HEADER = '{lineage} Duplicated BUSCO'
BUSCO_MISSING_HEADER = '{lineage} Missing BUSCO'
SUMMARY_REGEX = r'C:-?(\d+.\d)%\[S:-?(\d+.\d)%,D:-?(\d+.\d)%\],F:-?(\d+.\d)%,M:-?(\d+.\d)%,n:\d+'


# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    # Get parameters from snakemake object
    a3cat_file_path = snakemake.input.a3cat[0]
    busco_summary_file_paths = snakemake.input.busco_summary_files
    taxonomy_file_path = snakemake.input.taxonomy[0]
    assembly_stats_file_path = snakemake.input.assembly_stats[0]
    comparison_data_file_path = snakemake.output[0]
    taxonomical_level = snakemake.wildcards.level
    assembly_info = snakemake.params.assembly_info
    lineage_header = snakemake.params.lineage_header

    lineage_datasets = assembly_info[lineage_header]

    # Organize BUSCO summary files in a dictionary
    busco_summary_file_paths = {os.path.basename(os.path.dirname(p)): p for p in busco_summary_file_paths}

    taxonomical_level = nameify(taxonomical_level)

    logging.info('Loading species taxonomy')
    taxonomy = load_json_to_dict(taxonomy_file_path)
    try:
        species_taxonomical_level = taxonomy[taxonomical_level.lower()]
    except KeyError:
        logging.error(f'Invalid taxonomical level <{taxonomical_level}> when retrieving taxonomy data for the species.')
        exit(1)

    comparison_data = []
    logging.info('Retrieving data from A3Cat')
    with open(a3cat_file_path) as a3cat:
        header = a3cat.readline()[:-1].split('\t')
        assembly_size_index = header.index(ASSEMBLY_SIZE_HEADER)
        scaffold_n50_index = header.index(SCAFFOLD_N50_HEADER)
        scaffold_l50_index = header.index(SCAFFOLD_L50_HEADER)
        try:
            level_index = header.index(taxonomical_level)
        except ValueError:
            logging.error(f'Invalid taxonomical level <{taxonomical_level}> when retrieving comparison data.')
            exit(1)
        for line in a3cat:
            fields = line[:-1].split('\t')
            if fields[level_index] == species_taxonomical_level:
                current_data = [fields[assembly_size_index],
                                fields[scaffold_n50_index],
                                fields[scaffold_l50_index]]
                for lineage_dataset in lineage_datasets:
                    lineage_dataset = nameify(lineage_dataset)
                    busco_complete_index = header.index(BUSCO_COMPLETE_HEADER.format(lineage=lineage_dataset))
                    busco_fragmented_index = header.index(BUSCO_FRAGMENTED_HEADER.format(lineage=lineage_dataset))
                    busco_duplicated_index = header.index(BUSCO_DUPLICATED_HEADER.format(lineage=lineage_dataset))
                    busco_missing_index = header.index(BUSCO_MISSING_HEADER.format(lineage=lineage_dataset))
                    current_data += [fields[busco_complete_index],
                                     fields[busco_fragmented_index],
                                     fields[busco_duplicated_index],
                                     fields[busco_missing_index]]
                comparison_data.append(current_data)

    logging.info('Retrieving assembly stats for the focal species')
    assembly_stats = load_json_to_dict(assembly_stats_file_path)
    species_data = [assembly_stats['Scaffold Stats']['total_bps'],
                    assembly_stats['Scaffold Stats']['N50'],
                    assembly_stats['Scaffold Stats']['L50']]

    logging.info('Retrieving BUSCO results for the focal species')
    for lineage_dataset in lineage_datasets:
        with open(busco_summary_file_paths[lineage_dataset]) as busco_summary_file:
            found_results_line = False
            # Iterate over BUSCO summary files using a regex to parse BUSCO results
            for line in busco_summary_file:
                matches = re.search(SUMMARY_REGEX, line)
                if matches:
                    found_results_line = True
                    species_data += [float(matches.group(1)),
                                     float(matches.group(2)),
                                     float(matches.group(3)),
                                     float(matches.group(4)),
                                     float(matches.group(5))]
            if not found_results_line:
                logging.error(f'Could not find busco scores line in file <{busco_file_path}>')
                exit(1)

    logging.info(f'Saving comparison data to <{comparison_data_file_path}>')
    with open(comparison_data_file_path, 'w') as comparison_data_file:
        header_line_fields = [ASSEMBLY_SIZE_HEADER,
                              SCAFFOLD_N50_HEADER,
                              SCAFFOLD_L50_HEADER]
        for lineage_dataset in lineage_datasets:
            lineage_dataset = nameify(lineage_dataset)
            header_line_fields += [BUSCO_COMPLETE_HEADER.format(lineage=lineage_dataset),
                                   BUSCO_FRAGMENTED_HEADER.format(lineage=lineage_dataset),
                                   BUSCO_DUPLICATED_HEADER.format(lineage=lineage_dataset),
                                   BUSCO_MISSING_HEADER.format(lineage=lineage_dataset)]
        comparison_data_file.write('\t'.join([str(f) for f in header_line_fields]) + '\n')
        for line in comparison_data:
            comparison_data_file.write('\t'.join([str(f) for f in line]) + '\n')
        comparison_data_file.write('\t'.join([str(f) for f in species_data]) + '\n')

    logging.info(f'Successfully retrieved comparison data for level <{taxonomical_level}>')
